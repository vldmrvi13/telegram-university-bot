import datetime
import io
import os

import telebot
from models.user import *
from models.group import get_group_by_name
from models.teacher import get_teacher_by_name_surname
from models.event import get_all_events


class Bot:
    def __init__(self, config):
        self.tg_bot_token = config['telegram_bot_token']
        self.bot = telebot.TeleBot(self.tg_bot_token)

        @self.bot.message_handler(commands=['start'])
        def send_start(message):
            status = add_user(message.chat.id, False)
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(telebot.types.InlineKeyboardButton(text='test button'))
            self.bot.send_message(message.chat.id, 'Added' if status else 'Exists')

        @self.bot.message_handler(commands=['schedule_group'])
        def send_group_schedule(message):
            group_name = str(message.text.split(' ')[-1])
            session = Session()
            lessons = get_group_by_name(group_name, session).lessons
            upcoming_lessons = [lesson for lesson in lessons if lesson.event.dt > datetime.datetime.now()]
            for i in upcoming_lessons:
                upcoming_lesson_message = f'{i.subject.name}, {i.event.event_type.name}\nПреподаватель: {i.teacher.name} {i.teacher.surname} {f"(email: {i.teacher.email})" if i.teacher.email is not None else ""}\nМесто проведения: {i.event.location.location}, {f"ауд. {i.event.location.classroom}" if i.event.location.classroom is not None else i.event.location.description}\nВремя: {i.event.dt}'
                self.bot.send_message(message.chat.id, upcoming_lesson_message)
            session.close()

        @self.bot.message_handler(commands=['schedule_teacher'])
        def send_teacher_schedule(message):
            teacher_name = str(message.text.split(' ')[-2])
            teacher_surname = str(message.text.split(' ')[-1])
            session = Session()
            lessons = get_teacher_by_name_surname(teacher_name, teacher_surname, session).lessons
            upcoming_lessons = [lesson for lesson in lessons if lesson.event.dt > datetime.datetime.now()]
            for i in upcoming_lessons:
                upcoming_lesson_message = f'{i.subject.name}, {i.event.event_type.name}\nПреподаватель: {i.teacher.name} {i.teacher.surname} {f"(email: {i.teacher.email})" if i.teacher.email is not None else ""}\nМесто проведения: {i.event.location.location}, {f"ауд. {i.event.location.classroom}" if i.event.location.classroom is not None else i.event.location.description}\nВремя: {i.event.dt}'
                upcoming_lesson_message += '\nГруппы: ' + ','.join([group.name for group in i.groups])
                self.bot.send_message(message.chat.id, upcoming_lesson_message)
            session.close()

        @self.bot.message_handler(commands=['schedule_events'])
        def send_schedule(message):
            session = Session()
            events = get_all_events(session)
            upcoming_event = [event for event in events if event.dt > datetime.datetime.now()]
            for i in upcoming_event:
                upcoming_event_message = f'{i.event_type.name}: {i.description}\n {i.event_type.name}\nМесто проведения: {i.location.location}, {f"ауд. {i.location.classroom}" if i.location.classroom is not None else i.location.description}\nВремя: {i.dt}'
                self.bot.send_message(message.chat.id, upcoming_event_message)
            session.close()

        @self.bot.message_handler(commands=['templates'])
        def send_templates(message):
            templates = os.listdir('templates')
            self.bot.send_message(message.chat.id, 'Доступные шаблоны:\n' + '\n'.join(templates))

        @self.bot.message_handler(commands=['template'])
        def send_templates(message):
            template_name = str(message.text.split(' ')[-1])
            if not os.path.exists(f'templates/{template_name}'):
                self.bot.send_message(message.chat.id, 'Такого шаблона не существует')
                return

            with open(f'templates/{template_name}', 'rb') as doc:
                self.bot.send_document(message.chat.id, doc.read(), caption=template_name)

        @self.bot.message_handler(commands=['notify_all'])
        def send_templates(message):
            session = Session()

            message_text = str(message.text).replace('/notify_all', '')
            if not get_user(message.chat.id, session).is_admin:
                self.bot.send_message(message.chat.id, 'Недостаточно привилегий')
                return
            for user in get_all_users(session):
                self.bot.send_message(user.id, message_text)
            session.close()

    def start(self):
        self.bot.polling()
