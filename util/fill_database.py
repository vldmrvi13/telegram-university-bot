from models.event import Event
from models.event_type import EventType
from models.location import Location
from models.subject import Subject
from models.user import *
from models.group import Group
from models.lesson import Lesson
from models.teacher import Teacher
import datetime


def fill_users():
    session = Session()
    session.query(User).delete()
    session.commit()
    session.close()

    add_user(233857350, True)


def fill_groups():
    session = Session()
    session.query(Group).delete()
    groups = [
        Group('И973'),
        Group('И974'),
        Group('И975'),
        Group('И952')
    ]
    session.add_all(groups)
    session.commit()
    session.close()
    return groups


def fill_teachers():
    session = Session()
    session.query(Teacher).delete()
    teachers = [
        Teacher('Иван', 'Иванов', 'ivan@ivanov.ii'),
        Teacher('Александра', 'Александрова')
    ]
    session.add_all(teachers)
    session.commit()
    session.close()
    return teachers


def fill_subjects():
    session = Session()
    session.query(Subject).delete()
    subjects = [
        Subject('Программирование'),
        Subject('Линейная алгебра')
    ]
    session.add_all(subjects)
    session.commit()
    session.close()
    return subjects


def fill_event_types():
    session = Session()
    session.query(EventType).delete()
    event_types = [
        EventType('Лекция'),
        EventType('Практическое занятие'),
        EventType('Лабораторное занятие'),
        EventType('Конференция'),
        EventType('Встреча с администрацией')
    ]
    session.add_all(event_types)
    session.commit()
    session.close()
    return event_types


def fill_locations():
    session = Session()
    session.query(Location).delete()
    locations = [
        Location('ул. Победы, д.3, к.14', 54),
        Location('ул. Победы, д.3, к.14', 57),
        Location('наб. Обводоного канала, д.13', 11),
        Location('наб. Обводоного канала, д.13', None, 'Факультет компьютерных наук')
    ]
    session.add_all(locations)
    session.commit()
    session.close()
    return locations


def fill_events(event_types, locations):
    session = Session()
    session.query(Event).delete()
    events = [
        Event(event_types[3], locations[3], '3-я научная конференция', datetime.datetime(2021, 9, 3, 11, 0)),
        Event(event_types[1], locations[0], None, datetime.datetime(2021, 9, 5, 10, 0)),
        Event(event_types[0], locations[1], None, datetime.datetime(2021, 9, 5, 13, 0)),
        Event(event_types[0], locations[2], None, datetime.datetime(2021, 9, 6, 14, 0)),
        Event(event_types[1], locations[2], None, datetime.datetime(2021, 9, 6, 16, 0))
    ]
    session.add_all(events)
    session.commit()
    session.close()
    return events


def fill_lessons(groups, teachers, subjects, events):
    session = Session()
    session.query(Lesson).delete()
    lessons = [
        Lesson(groups, teachers[0], subjects[0], events[1]),
        Lesson(groups[:3], teachers[0], subjects[1], events[2]),
        Lesson([groups[0]], teachers[1], subjects[0], events[3]),
        Lesson(groups[:-2], teachers[1], subjects[1], events[4])
    ]
    session.add_all(lessons)
    session.commit()
    session.close()


def refill_database():
    fill_users()
    groups = fill_groups()
    teachers = fill_teachers()
    subjects = fill_subjects()
    event_types = fill_event_types()
    locations = fill_locations()

    events = fill_events(event_types, locations)
    fill_lessons(groups, teachers, subjects, events)

