import argparse
import yaml
from models.base import engine, Base
from models.event import get_all_events
from models.teacher import get_teacher_by_name_surname
from util import fill_database
from models.lesson import get_all_lessons
from bot import Bot


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', dest='config', required=False, default='config.yaml')
    parser.add_argument('-f', '--fill_db', dest='fill_db', required=False, action="store_true")
    arguments = parser.parse_args()
    with open(arguments.config, 'r') as config_file:
        config = yaml.safe_load(config_file)
    Base.metadata.create_all(engine)
    if arguments.fill_db:
        fill_database.refill_database()

    # lessons = get_all_lessons()
    # group = get_group_by_name('И973')
    # teacher = get_teacher_by_name_surname('Иван', 'Иванов')
    # events = get_all_non_lesson_events()
    tg_bot = Bot(config)
    tg_bot.start()


if __name__ == '__main__':
    main()
