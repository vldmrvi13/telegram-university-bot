from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import relationship

from models.base import Base, Session


class Teacher(Base):
    __tablename__ = 'teacher'
    id = Column('teacher_id', Integer, autoincrement=True, primary_key=True)
    name = Column('teacher_name', String, nullable=False)
    surname = Column('teacher_surname', String, nullable=False)
    email = Column('email', String)
    lessons = relationship(
        'Lesson',
        back_populates='teacher',
        lazy='joined'
    )

    def __init__(self, teacher_name, teacher_surname, email=None):
        self.name = teacher_name
        self.surname = teacher_surname
        self.email = email


def get_teacher_by_name_surname(teacher_name, teacher_surname, session=None):
    close_session = False
    if session is None:
        session = session = Session()
        close_session = True

    teacher = session.query(Teacher).filter(Teacher.name == teacher_name and Teacher.surname == teacher_surname).scalar()
    session.commit()

    if close_session:
        session.close()
    return teacher


