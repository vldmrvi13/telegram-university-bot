from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import relationship

from models.base import Base


class EventType(Base):
    __tablename__ = 'event_type'
    id = Column('event_type_id', Integer, autoincrement=True, primary_key=True)
    name = Column('event_type_name', String, nullable=False)

    def __init__(self, event_type_name):
        self.name = event_type_name




