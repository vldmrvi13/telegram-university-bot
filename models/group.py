from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import relationship

from models.base import Base, Session
from models.lesson import lesson_group_table


class Group(Base):
    __tablename__ = 'group'
    id = Column('group_id', Integer, autoincrement=True, primary_key=True)
    name = Column('group_name', String, nullable=False)
    lessons = relationship(
        'Lesson',
        secondary=lesson_group_table,
        back_populates='groups',
        lazy='joined'
    )

    def __init__(self, name):
        self.name = name


def get_group_by_name(group_name, session=None):
    close_session = False
    if session is None:
        session = session = Session()
        close_session = True

    group = session.query(Group).filter(Group.name == group_name).scalar()
    session.commit()

    if close_session:
        session.close()
    return group


