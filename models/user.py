from sqlalchemy import Column, String, Integer, Boolean

from models.base import Base, Session


class User(Base):
    __tablename__ = 'user'
    id = Column('user_id', Integer, primary_key=True, nullable=False)
    is_admin = Column('is_admin', Boolean, default=False)

    def __init__(self, id, is_admin):
        self.id = id
        self.is_admin = is_admin


def add_user(id, is_admin):
    session = Session()
    exists = session.query(User).get(id)
    if exists is None:
        session.add(User(id, is_admin))
    session.commit()
    session.close()
    return True if exists is None else False


def get_all_users(session=None):
    close_session = False
    if session is None:
        session = Session()
        close_session = True

    users = session.query(User).all()
    session.commit()

    if close_session:
        session.close()
    return users


def get_user(user_id, session=None):
    close_session = False
    if session is None:
        session = Session()
        close_session = True

    user = session.query(User).get(user_id)
    session.commit()

    if close_session:
        session.close()
    return user



