from sqlalchemy import Column, Integer, Boolean, ForeignKey, Table
from sqlalchemy.orm import relationship, joinedload

from models.base import Base, Session


lesson_group_table = Table(
    'lesson_group', Base.metadata,
    Column('lesson_id', Integer, ForeignKey('lesson.lesson_id')),
    Column('group_id', Integer, ForeignKey('group.group_id'))
)


class Lesson(Base):
    __tablename__ = 'lesson'
    id = Column('lesson_id', Integer, autoincrement=True, primary_key=True)
    teacher_id = Column('teacher_id', Integer, ForeignKey('teacher.teacher_id'))
    subject_id = Column('subject_id', Integer, ForeignKey('subject.subject_id'))
    event_id = Column('event_id', Integer, ForeignKey('event.event_id'))
    teacher = relationship(
        'Teacher',
        back_populates='lessons',
        lazy='joined'
    )
    subject = relationship(
        'Subject',
        back_populates='lessons',
        lazy='joined'
    )
    event = relationship(
        'Event',
        lazy='joined'
    )
    groups = relationship(
        'Group',
        secondary=lesson_group_table,
        back_populates='lessons',
        lazy='joined'
    )

    def __init__(self, groups, teacher, subject, event):
        self.groups = groups
        self.teacher = teacher
        self.subject = subject
        self.event = event


def get_all_lessons():
    session = Session()
    lessons = session.query(Lesson).all()
    session.commit()
    session.close()
    return lessons



