from sqlalchemy import Column, Integer, Boolean, ForeignKey, Table, DateTime, String
from sqlalchemy.orm import relationship, joinedload

from models.base import Base, Session
from models.event_type import EventType


class Event(Base):
    __tablename__ = 'event'
    id = Column('event_id', Integer, autoincrement=True, primary_key=True)
    dt = Column('event_dt', DateTime, nullable=False)
    location_id = Column('location_id', Integer, ForeignKey("location.location_id"), nullable=False)
    event_type_id = Column('event_type_id', Integer, ForeignKey("event_type.event_type_id"), nullable=False)
    description = Column('description', String)

    location = relationship("Location", uselist=False, lazy='joined')
    event_type = relationship("EventType", uselist=False, lazy='joined')

    def __init__(self, event_type, location, description, dt):
        self.location = location
        self.event_type = event_type
        self.description = description
        self.dt = dt


def get_all_events(session=None):
    close_session = False
    if session is None:
        session = session = Session()
        close_session = True
    events = session.query(Event).join(EventType).filter(EventType.name == 'Конференция').all()
    session.commit()
    if close_session:
        session.close()
    return events
