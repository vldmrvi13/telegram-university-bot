from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import relationship

from models.base import Base


class Subject(Base):
    __tablename__ = 'subject'
    id = Column('subject_id', Integer, autoincrement=True, primary_key=True)
    name = Column('subject_name', String, nullable=False)
    lessons = relationship(
        'Lesson',
        back_populates='subject',
        lazy='joined'
    )

    def __init__(self, subject_name):
        self.name = subject_name




