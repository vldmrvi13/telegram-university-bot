from sqlalchemy import Column, String, Integer, Boolean

from models.base import Base


class Location(Base):
    __tablename__ = 'location'
    id = Column('location_id', Integer, autoincrement=True, primary_key=True)
    location = Column('location', String)
    classroom = Column('classroom', Integer)
    description = Column('address', String)

    def __init__(self, location, classroom=None, description=None):
        self.location = location
        self.classroom = classroom
        self.description = description




